from django.template.response import TemplateResponse
from django.shortcuts import render

from ..product.models import Product


def home(request):
    products = Product.objects.get_available_products()[:12]
    products = products.prefetch_related('categories', 'images',
                                         'variants__stock')
    return TemplateResponse(
        request, 'index.html',
        {'products': products, 'parent': None})


def contact(request):
    return render(request, "contact.html", {
    "title":"Contact SuitsByYou",
    })
