from django.contrib import admin
from mptt.admin import MPTTModelAdmin

from ..discount.models import Sale
from .models import (ProductImage, Category)
from .models.base import FabricImages, CustomizeImage, Customization, EventImage, Event
from .forms import ImageInline


class ImageAdminInline(admin.StackedInline):
    model = ProductImage
    formset = ImageInline


class ProductCollectionAdmin(admin.ModelAdmin):
    search_fields = ['name']


class CustomizeImageInline(admin.StackedInline):
    model = CustomizeImage
    extra = 1

class CustomizeAdmin(admin.ModelAdmin):
    inlines = [CustomizeImageInline]


class EventImageInline(admin.StackedInline):
    model = EventImage
    extra = 1

class EventAdmin(admin.ModelAdmin):
    inlines = [EventImageInline]

admin.site.register(Category, MPTTModelAdmin)
admin.site.register(Sale)
admin.site.register(FabricImages)
admin.site.register(Customization, CustomizeAdmin)
admin.site.register(Event, EventAdmin)

from django.contrib import admin
from django.contrib.flatpages.admin import FlatPageAdmin
from django.contrib.flatpages.models import FlatPage
from django.db import models

from ckeditor.widgets import CKEditorWidget

class FlatPageCustom(FlatPageAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget}
    }

admin.site.unregister(FlatPage)
admin.site.register(FlatPage, FlatPageCustom)
